<?php
    function validarNom($nom){
        if ($nom == null){
            return "<br><p style='color: red;'>Has d'introduir un nom</p>";
        }
        elseif (strlen($nom) <= 3){
            return "<br><p style='color: red;'>Ha de tenir mes de 3 caracters</p>";
        }
        else{
            return false;
        }
    }
    function validarEmail($email){
        if ($email == null) {
            return "<br><p style='color: red;'>Has d'introduir un mail</p>";
        }
        else {
            $arrobapunts = "";
            $arroba = false;
            $formatCorrecte = true;
            for ($i = 0; $i < strlen($email) && $formatCorrecte; $i++){
                if (($email[$i] == '@') || ($email[$i] == '.')){
                    $arrobapunts .= $email[$i];
                    if ($email[$i] == '@'){
                        if ($arroba == true){
                            $formatCorrecte = false;
                        }
                        $arroba = true;
                    }

                    if (!ctype_alnum($email[$i-1]) && !ctype_alnum($email[$i+1])){
                        $formatCorrecte = false;

                    }
                    elseif ($i == 0 || $i == strlen($email)){
                        $formatCorrecte = false;
                    }
                }
                elseif (!ctype_alnum($email[$i])){
                    $formatCorrecte = false;
                }
            }
            if ($formatCorrecte && ($arrobapunts[-1] != '.' || !$arroba)){
                $formatCorrecte = false;
            }
            return ($formatCorrecte ? false : "<br><p style='color: red;'>Format incorrecte de email</p>");
        }

    }
?>