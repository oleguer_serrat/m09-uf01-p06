<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validacions</title>
</head>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f4f4f4;
    }

    form {
        max-width: 600px;
        margin: 20px auto;
        padding: 20px;
        background-color: #fff;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    p {
        margin-bottom: 10px;
    }

    input,
    select {
        padding: 10px;
        margin-bottom: 5px;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    select {
        height: 40px;
    }

    input[type="submit"] {
        background-color: #4caf50;
        color: #fff;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        background-color: #45a049;
    }

    #missatge {
        width: 96%;
        height: 300px;
    }

    .error {
        color: red;
    }
</style>

<body>
    <form action="#" method="post">
        <p>Nom</p>
        <input type="text" name="nom" placeholder="Nom">
        <p> </p>
        </><input type="text" name="cognom" placeholder="Cognoms">
        <p>E-mail</p>
        <input type="mail" name="email" placeholder="Email">
        <p>Telefon</p>
        <input type="number" name="telefon" placeholder="### ### ###">
        <p>Sexe</p>
        <p><input type="radio" name="gender" value="femeni">Femeni</p>
        <p><input type="radio" name="gender" value="masculi">Masculi</p>
        <p><input type="radio" name="gender" value="altres">Altres</p>

        <p>Idioma</p>


        <select name="Idioma">
            <option value="" disabled selected>Escull</option>
            <option value="Espanyol">Espanyol</option>
            <option value="Catala">Cataka</option>
            <option value="Angles">Anglès</option>
        </select>



        <p>Adreça</p>
        <input type="text" name="carrer" placeholder="Carrer">
        <input type="number" name="num" placeholder="Numero">

        <br>

        <input type="text" name="ciutat" placeholder="Ciutat">
        <input type="text" name="provincia" placeholder="Provincia">

        <br>

        <input type="cp" name="num" placeholder="Codia Postal">
        <select name="pais">
            <option value="" disabled selected>Escull un país</option>
            <option value="Espanya">Afghanistan</option>
            <option value="Marroc">Albania</option>
            <option value="Algeria">Algeria</option>
            <option value="Andorra">Andorra</option>
            <option value="...">Angola</option>
        </select>

        <br>

        <p>Informacio de Mes</p>

        <input type="text" name="provincia" id='missatge' placeholder="Missage...">

        <br>
        <input type="submit" value="Enviar">
        <br>
        <?php
        function validarEmail($email)
        {
            if ($email == null) {
                return "<p class='error'>Has d'introduir un mail</p>";
            } else {
                $arrobapunts = "";
                $arroba = false;
                $formatCorrecte = true;
                for ($i = 0; $i < strlen($email) && $formatCorrecte; $i++) {
                    if (($email[$i] == '@') || ($email[$i] == '.')) {
                        $arrobapunts .= $email[$i];
                        if ($email[$i] == '@') {
                            if ($arroba == true) {
                                $formatCorrecte = false;
                            }
                            $arroba = true;
                        }

                        if (!ctype_alnum($email[$i - 1]) && !ctype_alnum($email[$i + 1])) {
                            $formatCorrecte = false;

                        } elseif ($i == 0 || $i == strlen($email)) {
                            $formatCorrecte = false;
                        }
                    } elseif (!ctype_alnum($email[$i])) {
                        $formatCorrecte = false;
                    }
                }
                if ($formatCorrecte && ($arrobapunts[-1] != '.' || !$arroba)) {
                    $formatCorrecte = false;
                }
                return ($formatCorrecte ? false : "<p class='error'>Format incorrecte de email</p>");
            }

        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $nom = $_POST["nom"];
            $cognom = $_POST["cognom"];
            $email = $_POST["email"];
            $telefon = $_POST["telefon"];
            $gender = $_POST["gender"];
            $idioma = $_POST["Idioma"];
            $carrer = $_POST["carrer"];
            $num = $_POST["num"];
            $ciutat = $_POST["ciutat"];
            $provincia = $_POST["provincia"];
            $codipostal = $_POST["cp"];
            $informacio = $_POST["missatge"];

            if ($nom === '' || $cognom === '' || strlen($nom) <= 5 || strlen($cognom) <= 5 || is_numeric($nom) || is_numeric($cognom)) {
                echo "<p class='error'>El nom i cognom han de ser no nuls, tenir una llargada major que 5 i no poden ser caràcters numèrics.</p>";
            }

            $sexesPermesos = array(
                "Masculí",
                "Femení",
                "Altres"
            );
            if (!in_array($gender, $sexesPermesos)) {
                echo "<p class='error'>Si us plau, selecciona un valor vàlid per al camp sexe.</p>";
            }

            $idiomesPermesos = array(
                "Català",
                "Espanyol",
                "Anglès"
            );
            if ($idioma === 'escull' || !in_array($idioma, $idiomesPermesos)) {
                echo "<p class='error'>Si us plau, selecciona un idioma vàlid.</p>";
            }

            // he utilizat la validacio de l'exercici anterior
            echo validarEmail($email);

            if ($carrer === '' || strlen($carrer) <= 5) {
                echo "<p class='error'>El carrer ha de ser no nul i tenir una llargada major que 5.</p>";
            }

            if ($num === '' || !is_numeric($num)) {
                echo "<p class='error'>El número ha de ser no nul i ser un valor numèric.</p>";
            }

            if ($ciutat === '' || strlen($ciutat) <= 3) {
                echo "<p class='error'>La ciutat ha de ser no nula i tenir una llargada major que 3.</p>";
            }

            if ($provincia === '' || !in_array($provincia, $provinciesPermeses)) {
                echo "<p class='error'>La provincia ha de ser no nula i ser una província vàlida.</p>";
            }

            if ($codipostal === '' || !is_numeric($codipostal)) {
                echo "<p class='error'>El codi postal ha de ser no nul i ser un valor numèric.</p>";
            }

            if (strlen($informacio) <= 4 || strlen($informacio) > 256) {
                echo "<p class='error'>La informació ha de tenir un contingut de mida menor a 256 caràcters i major que 4.</p>";
            }
        }
        ?>

</body>

</html>
